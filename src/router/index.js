import { createRouter, createWebHistory } from 'vue-router'
import project from '@/views/project/project.vue'

const routes = [
  {
    path: '/',
    name: 'project',
    component: project
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
